#!/usr/bin/env python
import Adafruit_BBIO.ADC as ADC
import rospy
from test_1.msg import PhotoData
ADC.setup()

def talker():
    pub = rospy.Publisher("fpmg_raw", PhotoData, queue_size=10)
    rospy.init_node("talker", anonymous=True)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        data = PhotoData(rospy.Time.now(), ADC.read("P9_40"))
        pub.publish(data)
        rate.sleep();

if __name__ == "__main__":
    try:
        talker()
    except rospy.ROSInterrupedException:
        pass
