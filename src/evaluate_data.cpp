#include "ros/ros.h"
#include "ros/time.h"
#include "test_1/PhotoData.h"

void spitOutData(const test_1::PhotoData o){
	ROS_INFO("Going backWards -[%f], and its been \n [%d s%d nsec]", o.volt, o.time.sec, o.time.nsec);
}

int main(int argc, char **argv){
	ros::init(argc, argv, "evaluate_data");

	ros::NodeHandle n;

	ros::Subscriber sub = n.subscribe("fpmg_filtered", 1000, spitOutData);
	ros::spin();


	return 0;
}
