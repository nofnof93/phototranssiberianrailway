#include "ros/ros.h"
#include <deque>
#include "test_1/PhotoData.h"
using namespace std;
ros::Publisher pub;

deque<double> raw_data;
deque<double> filter_data;

int counter = 0;

double filter(deque<double> S, deque<double> y){
	 double filtered = 1.25*y[0] - 0.53*y[1] + 0.15*S[0] + 0.27*S[1];
	 filter_data.push_back(filtered);
	 filter_data.pop_front();
	 ROS_INFO("RawData: %f \n FilteredData: %f", raw_data.front(), filtered);
	return filtered;
}
void chatterCallback(const test_1::PhotoData::ConstPtr& o ){
	if(raw_data.size() < 2){
		raw_data.push_back(o->volt);
	}else{
		raw_data.pop_front();
	}
	if(filter_data.size()<2){
		filter_data.push_back(o->volt);
	}
	if(raw_data.size() == 2){
		test_1::PhotoData data;
		data.time = o->time;
		data.volt = filter(raw_data, filter_data);

		pub.publish(data);
	}
}

int main(int argc, char **argv){
	ros::init(argc, argv, "listener");

	ros::NodeHandle n;

	ros::Subscriber sub = n.subscribe("fpmg_raw", 1000, chatterCallback);
	pub = n.advertise<test_1::PhotoData>("fpmg_filtered",1000);
	ros::spin();


	return 0;
}
