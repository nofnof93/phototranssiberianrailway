#include "ros/ros.h"
#include "test_1/PhotoData.h"

int main(int argc, char **argv){
	ros::init(argc, argv, "talker");
	ros::NodeHandle n;
	ros::Publisher publisher = n.advertise<test_1::PhotoData>("fpmg_raw", 1000);
	ros::Rate loop_rate(10);

	int count = 0;
	while (ros::ok()){
		test_1::PhotoData o;
		o.time = ros::Time::now();
		o.volt = 0.1;


		publisher.publish(o);

		ros::spinOnce();
		loop_rate.sleep();
		count++;
	}
	return 0;
}
